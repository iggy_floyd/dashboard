
<?php

/**
 * database.php
 *
 * application database configuration
 *
 * @package		TinyMVC
 */

$config['default']['plugin'] = 'TinyMVC_PDO'; // plugin for db access
$config['default']['type'] = 'sql';      // connection type
$config['default']['host'] = 'localhost';  // db hostname
$config['default']['name'] = 'dbname';     // db name
$config['default']['user'] = 'dbuser';     // db username
$config['default']['pass'] = 'dbpass';     // db password
$config['default']['persistent'] = false;  // db connection persistence?


// FPS detector settings
$config['fpsdetector']['plugin'] = 'TinyMVC_FPSDetector'; // plugin to communicate with FPS ML detector 
$config['fpsdetector']['host'] = 'localhost';  // RPC Service hostname 
$config['fpsdetector']['port'] = 8092;  // RPC Service port



