<?php

/**
 * autoload.php
 *
 * application auto-loaded plugin configuration
 *
 * @package		TinyMVC
 * @author		Monte Ohrt
 */


/* auto-loaded libraries */
$config['libraries'] = array(
array('TBS_Wrapper','tbs'),
array('JForm_Wrapper','jform')
);

/* auto-loaded scripts */
$config['scripts'] = array();


?>
