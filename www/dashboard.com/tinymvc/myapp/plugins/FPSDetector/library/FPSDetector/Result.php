<?php

/*
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */

/**
 * FPSDetector_Result represents a FPS ML Detector response
 *
 * @author Igor Marfin
 */
class FPSDetector_Result extends ArrayIterator
{

    private $_result = null;

    public function __construct($result)
    {

        if (array_key_exists('result', (array) $result)) {            
            
            $this->_result = json_decode($result['result'],true);                        
            return parent::__construct((Array) $this->_result);
            
        } else {
            $this->_result = array();
            return parent::__construct(array());
        }
    }


    public function getPredictions()
    {
        if (array_key_exists('prediction', $this->_result)) {
            return $this->_result['prediction'];
        } else {
            return array();
        }
    }
    
    
    public function getStatus()
    {
        if (array_key_exists('status', $this->_result)) {
            return $this->_result['status'];
        } else {
            return array();
        }
    }
  
      public function getError()
    {
        if (array_key_exists('error', $this->_result)) {
            return $this->_result['error'];
        } else {
            return array();
        }
    }
    
  
    
    public function __toString()
    {
        return implode('\n', $this->_result);
    }

}
