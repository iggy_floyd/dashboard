<?php

class FPSDetector_Client
{
    /* @var URL_READ_GREMLIN  FPSDetector_Client */
    const URL_COMMUNICATE = '';

    
    /* @var $_adapter FPSDetector_Client */
    private $_adapter = null;

 
    /**
     * 
     * 
     * @param Cayley_Client_Adapter_Interface $adapter_w
     */
    public function __construct(FPSDetector_Client_Adapter_Interface $adapter)
    {
        $this->_adapter = $adapter;
        set_time_limit(0); // to infinity
    }

    /** performs an initialization of the FPS detector: 
     *  1) collect data from $file_data_base
     *  2) calculate new features (aka transformation step)
     *  3) create a dataframe to be used during the training
     * 
     * @param string $file_data_base
     * @return FPSDetector_Result
     */
    public function initProcess($file_data_base) {
        return $this->execute(array($file_data_base),'init_process');
    }
    
    
    /**
     * performs an initialization of the Model Features
     * 
     * @return FPSDetector_Result
     */
    public function initModelFeatures() {
        return $this->execute(array(),'init_model_features');
    }

     /**
     * adds classifiers to the train chain
     * 
     * @param string $type  -- type of the classifier
     * @param string $name -- name of the classifier
     * @return FPSDetector_Result
     */
    public function addClassifier($type,$name) {
                
        return $this->execute(array($type,$name),'add_classifier');
    }

    /**
     * trains (fits) classifiers to the train chain
     * 
     * @return FPSDetector_Result
     */
    public function trainClassifiers() {
        return $this->execute(array(),'train_classifiers');
    }
    
    /**
     * simulate FPS requests and check output of the FPS detector     
     * 
     * @param string $data  --data of the booking
     * @param string $truevalue -- 'status' of the booking
     * 
     * @return FPSDetector_Result
     */
    public function predictValidation($data,$truevalue) {
        return $this->execute(array($data,$truevalue),'predict_validation');        
    }
    
    
    /**
     * execute a method
     * @param  array         $params
     * @param  string         $method
     * @return FPSDetector_Result
     */
    public function execute($params, $method)
    {
        $request = array(
            'jsonrpc' =>'2.0',
            'id' => '1',
            'method' => $method,
            'params' =>$params            
        );
                
        $response = $this->_doRequest($this->_adapter, self::URL_COMMUNICATE, json_encode($request));
        return new FPSDetector_Result(json_decode($response, true));
        
    }

   

    /**
     * send query to the service
     * @param  FPSDetector_Client_Adapter_Interface                           $client
     * @param  string                           $url
     * @param  string                           $body
     * @return string
     * @throws Cayley_Client_Exception_HttpException
     */
    private function _doRequest(FPSDetector_Client_Adapter_Interface $client, $url, $body)
    {
        try {
            return $client->query($url, $body);
        } catch (Exception $e) {
            throw new FPSDetector_Client_Exception_HttpException($e);
        }
    }

}
