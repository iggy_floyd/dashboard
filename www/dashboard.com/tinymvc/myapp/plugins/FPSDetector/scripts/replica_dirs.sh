#
#  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
#  All rights reserved.
#

# Makes an exact replica of the directory structure ('library', in the example below)
#
# Example of use
#   
#       cd /home/vagrant/project/bug/bug248776/library/cayley;
#       tree -d    -X  --noreport  -o dirstr library
#       cp dirstr /path/to/my/own/scratch/
#       cd /path/to/my/own/scratch/
#       source replica_dirs dirstr | xargs -I {} echo {} | bash
#


# function to trim whitespaces
function trim() {
    local var=$@
    var="${var#"${var%%[![:space:]]*}"}"   # remove leading whitespace characters
    var="${var%"${var##*[![:space:]]}"}"   # remove trailing whitespace characters
    echo -n "$var"
}




# to create directory structure
function read_dir_structure
{

    local depth;
    declare -A map; # associative array

    oIFS="$IFS"
    IFS=$'\n'


    for str in  `awk '/<directory/,/<\/directory>/' $1  | sed  -e 's/<directory /\//' -e '/<\/directory>/d' -e 's/name="\(.*\)">/\1/'`
    do

        depth=`echo "$str" | awk '{ gsub("[^ ]", ""); print length } '`;
        map[$depth]=`trim $str`;

        map[2]=${map[2]#'/'};

        if [ -n ${map[$((depth -2 ))]} ]
            then 
                echo mkdir -p ${map[$((depth -2 ))]}${map[$depth]};
                map[$depth]=${map[$((depth -2 ))]}${map[$depth]};
        fi


    done


    IFS="$oIFS"

}

# we expect only one parameter
read_dir_structure $1;
