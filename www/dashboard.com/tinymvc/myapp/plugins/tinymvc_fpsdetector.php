<?php

/* 
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */





/* * *
 * Name:       TinyMVC_FPSDetector
 * * */

// ------------------------------------------------------------------------

include_once dirname(__FILE__) . '/FPSDetector/SplClassLoader.php';



/**
 * 
 * Wrapper of the Communicator with FPS ML detector
 *
 * @author Igor Marfin
 */
class TinyMVC_FPSDetector {

    /**
     * 
     *
     * $client handle
     *
     * @access	public
     */
    public $client = null;
    public $classLoader = null;
    
    
    /**
     * class constructor
     *
     * @access	public
     */
    function __construct($config) {

        
        
        $classLoader = new SplClassLoader(null, dirname(__FILE__) . "/FPSDetector/library/");
        $classLoader->register();

        
        
         

        
        
        /**
         * 
         * here some sanity checks are performed
         * 
         */
        if (empty($config))
            throw new Exception("database definitions required.");
        
        
        if(!class_exists('FPSDetector_KLogger',true)) {
		throw new Exception("PHP FPSDetector package is required.");
        }
        if(!class_exists('FPSDetector_Debugger',true)) {
		throw new Exception("PHP FPSDetector package is required.");
        }
        if(!class_exists('FPSDetector_Result',true)) {
		throw new Exception("PHP FPSDetector package is required.");
        }        
        if(!class_exists('FPSDetector_Client_Adapter_Http',true)) {
		throw new Exception("PHP FPSDetector package is required.");
        }
        
        
       
        
        // to start the FPSDetector_Debugger  to report any problems
        // for debugging purpose, it will be removed in the release
        #FPSDetector_Debugger::reporting( E_ALL ); 
        
        FPSDetector_Debugger::reporting( null ); 
        FPSDetector_Debugger::logging( null ); 
        try {
            $this->client = new FPSDetector_Client(
                     
                        FPSDetector_Client_Adapter_Http::connect($config['host'], $config['port'])->setHeaders(array('Content-Type: text/plain'))
                    
                    );
            } catch (Exception $e) {
            throw new Exception(sprintf("Can't connect to the FPS detector.  Error: %s", $e->getMessage()));
        }

         /*
         * Here some tests of the classes of FPSDetectors are defined.
         * 
         * It's recommended to perform them in the 'demos/' test suite. Hovewer, if you are willing, you can
         * uncomment the following lines to test some features immedetely.
         * 
         */

        // Test 1: test of the result iterator
        
        FPSDetector_Debugger::log('Test 1: test of the result iterator');
        $result = json_decode(' { "jsonrpc" => "2.0", "id" => "1", "method" => "init_process",
                 "result":{"status":0,"result":{"prediction":[]}}}',True);                
        $qr = new FPSDetector_Result($result);        
        FPSDetector_Debugger::log($qr);
        
        
        
        // Test 2: test of the Http adapter
        /*
        // test correct rpc path
        $request = array(
            'jsonrpc' =>'2.0',
            'id' => '1',
            'method' => 'init_process',
            'params' => array('fps_data_3.json')
            
        );
        
        
        
        FPSDetector_Debugger::log('Test 2.1: test of the Http adapter: init_process');
        $result = json_decode(
                FPSDetector_Client_Adapter_Http::connect($config['host'], $config['port'])
                ->setHeaders(array('Content-Type: text/plain'))
                ->doPost('', json_encode($request)),true                
                );
        $qr = new FPSDetector_Result($result); 
        FPSDetector_Debugger::log($qr);
        
        
        // test incorrect rpc path
        $request = array(
            'jsonrpc' =>'2.0',
            'id' => '1',
            'method' => 'init_process2',
            'params' => array('fps_data_3.json')
            
        );
        
        
        
        FPSDetector_Debugger::log('Test 2.2: test of the Http adapter: init_process2');
        
       try {
            // might throw the Exception in case of INTERNAL_ERROR or NOT_FOUND
            $result = json_decode(
                FPSDetector_Client_Adapter_Http::connect($config['host'], $config['port'])
                ->setHeaders(array('Content-Type: text/plain'))
                ->doPost('', json_encode($request)),true                
                );
            $qr = new FPSDetector_Result($result); 
            FPSDetector_Debugger::log($qr);
        
        } catch (Exception $e) {
                FPSDetector_Debugger::log($e);
        }

        
        
        // test rpc path which is not ready
        $request = array(
            'jsonrpc' =>'2.0',
            'id' => '1',
            'method' => 'init_model_features',
            'params' => array()
            
        );
        
        
        
        FPSDetector_Debugger::log('Test 2.3: test of the Http adapter: init_model_features');
        try {
            
            // might throw the Exception in case of INTERNAL_ERROR or NOT_FOUND
            $result = json_decode(
                FPSDetector_Client_Adapter_Http::connect($config['host'], $config['port'])
                ->setHeaders(array('Content-Type: text/plain'))
                ->doPost('', json_encode($request)),true                
                );
            $qr = new FPSDetector_Result($result); 
            FPSDetector_Debugger::log($qr);
        } catch (Exception $ex) {
            FPSDetector_Debugger::log($ex);
        }

        
        
        $request = array(
            'jsonrpc' =>'2.0',
            'id' => '1',
            'method' => 'add_classifier',
            'params' => array('RandomForest', 'RandomForest1')
            
        );
        
        
        
        FPSDetector_Debugger::log('Test 2.4: test of the Http adapter:add_classifier');
        try {
            
            // might throw the Exception in case of INTERNAL_ERROR or NOT_FOUND
            $result = json_decode(
                FPSDetector_Client_Adapter_Http::connect($config['host'], $config['port'])
                ->setHeaders(array('Content-Type: text/plain'))
                ->doPost('', json_encode($request)),true                
                );
            $qr = new FPSDetector_Result($result); 
            FPSDetector_Debugger::log($qr);
        } catch (Exception $ex) {
            FPSDetector_Debugger::log($ex);
        }
        
        */
        
        /*
        $request = array(
            'jsonrpc' =>'2.0',
            'id' => '1',
            'method' => 'train_classifiers',
            'params' => array()
            
        );
        
        
        
        FPSDetector_Debugger::log('Test 2.5: test of the Http adapter: train_classifiers');        
        try {
            
            // might throw the Exception in case of INTERNAL_ERROR or NOT_FOUND
            $result = json_decode(
                FPSDetector_Client_Adapter_Http::connect($config['host'], $config['port'])
                ->setHeaders(array('Content-Type: text/plain'))
                ->doPost('', json_encode($request)),true                
                );
            $qr = new FPSDetector_Result($result); 
            FPSDetector_Debugger::log($qr);
        } catch (Exception $ex) {
            FPSDetector_Debugger::log($ex);
        }
        */
        
         
        // Test 3: test of the Http adapter: simulate FPS requests
        //$FPSrequests = file_get_contents( preg_replace('/www(.*)/','',dirname(__FILE__)).'/fps_data_4.json');
        /*$file_handle = fopen( preg_replace('/www(.*)/','',dirname(__FILE__)).'/fps_data_4.json', "r");
        while (!feof($file_handle)) {
            $line = fgets($file_handle);
            $line=json_decode($line, true);
            if (is_null($line)) {
                break;
            }
            
            
            
            $request = array(
            'jsonrpc' =>'2.0',
            'id' => '1',
            'method' => 'predict',
            'params' => array($line)            
            # 'params' => array(json_decode($line, true))            
        );
            
             FPSDetector_Debugger::log('Test 3: test of the Http adapter: simulate FPS');        
            try {
            
            // might throw the Exception in case of INTERNAL_ERROR or NOT_FOUND
            $result = json_decode(
                FPSDetector_Client_Adapter_Http::connect($config['host'], $config['port'])
                ->setHeaders(array('Content-Type: text/plain'))
                ->doPost('', json_encode($request)),true                
                );
            $qr = new FPSDetector_Result($result); 
            FPSDetector_Debugger::log($qr);
        } catch (Exception $ex) {
            FPSDetector_Debugger::log($ex);
        }
             
            
        }
        
        
        fclose($file_handle);
        */
    }
}
    