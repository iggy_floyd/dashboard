<?php

/*
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */

/**
 * Description of FPSDetector_Controller:
 * 
 * The main controller for the Dashboard
 *
 * @author Igor Marfin
 */
class Dashboard_Controller extends TinyMVC_Controller
{

      function index()
     {
          
          /*
           *  Model initialization
           * 
           */
          
          //load Dashboard_Model to the controller.          
          //$this->load->model('Dashboard_Model','fpsdetector',null,'dashboard');
          
          

          /*
           * Some needed variables to be rendered at the pages
           * 
           */
            
     
           /*
            * 
            * Forms initialization
            * 
            * 
            */
           $this->load->model('Dashboard_Forms_Model','forms_model');           
           $forms = $this->forms_model->get_forms();
           
           foreach($this->forms_model->forms as $name) {
               $this->tbs->VarRef[$name] =  $forms[$name]->getHtml();
           }
           
           
           /**
            * 
            * Here controller decides what to do depending on the user input
            * 
            */
           
           $response_rpc=null; // response from RPC server
           $exceptions  = array();
           
           // Case of the 'Dashboard Test'
           $name_form = 'dashboard_test_form';                      
           if( $forms[$name_form]->isSubmittedAndValid() ) {                              
               $form_values = array();
               $_vals =  $forms[$name_form]->getValues();

               foreach ($this->tbs->VarRef[$name_form]['inputs'] as $item) {
                   $label = preg_replace('/ /','_',$item["label"]);                  
                    $form_values[$label] = $_vals[$label];                    
		}                
           }
           
           
         
           
          
          
          /*
           * 
           *  View Layer definitions
           * 
           */
          

           // display the view layer with substitution templates 
	   $this->tbs->LoadTemplate('dashboard_view'); # load a template
           
           // render forms defined previously
           foreach($this->forms_model->forms as $name) {
               $this->tbs->MergeBlock($name, 'array', $name.'[inputs]'); // to produce the form from a template
           }
           
           
           // add a table with exceptions caught
           $this->tbs->MergeBlock('excpt', $exceptions); // to fill the template of the  table in the view layer           
           $this->tbs->Show();
      }
    //put your code here
}
