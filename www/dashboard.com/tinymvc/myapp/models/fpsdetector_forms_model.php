<?php

/* 
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */


// some trick: Now a model can load  any plugin :-)
class FPSDetector_Forms_Model extends TinyMVC_Controller
{

    public $forms=array(
            
        'prepare_detector_form',
        'simulate_detector_form'
        
    );
    
    function get_forms() {


        
        // Now this model can generate forms
        $this->load->library('JForm_Wrapper','jform');
        
        
        // 'prepare_detector_form'
        $this->jform->setForm(
                    array(
			'focus' => "", // focus first element
                        'submit_label' => "Submit",
                        'inputs' => array(                            
                            
                            'Name_Of_The_Classifier' => array(                                
                                    'type' => "text", // default
                                    //'required' => "true",
                                    'style' => "border-color: red;", // custom options for the <input> tag are added simply by
                                                                    // adding them to this array
				 ),
                            
                            'Classifier' => array(
                                    'type' => "select",
                                    'options' => array(
                                        'RandomForest' => array(),
                                        ),
                                    'style' => "border-color: red;", // custom options for the <input> tag are added simply by
                                                            // adding them to this array
			        ),
                                       
                            
                            'Dataset_File' => array(                                
                                    'type' => "file", 
                                    //'required' => "true",
                                    'style' => "border-color: red;", // custom options for the <input> tag are added simply by
                                                                    // adding them to this array
				 ),
                            
                            'Train_Sample_Size' => array(
                                    //'validators' => new NumericValidator(),
                                    //'required' => "true",
			            'maxlength' => 4, // automatically adds a CutOffFilter
			            'style' => "border-color: red;", // custom options for the <input> tag are added simply by
			            // adding them to this array
					        ),
                            'Hidden' => array(
                                    'type' => "hidden", 			            
			            	        ),
                            
                        )                        
                    )                  
            );
        
        
                      
            // initial values of the form 'prepare_detector_form'
	    $this->jform->setDefaultValues(array(
                    'Name_Of_The_Classifier' => "Classifier0",
		    'Classifier' => "RandomForest",
		    'Dataset_File' => "Unknown",
		    'Train_Sample_Size' => "0.5",
                
	    ));


            $forms['prepare_detector_form'] =   clone  $this->jform;
                
        
            // 'simulate_detector_form'
            $this->jform->setForm(
                    array(
			'focus' => "", // focus first element
                        'submit_label' => "Submit",
                        'inputs' => array(  
                            
                             'Dataset_File' => array(                                
                                    'type' => "file", 
                                    //'required' => "true",
                                    'style' => "border-color: red;", // custom options for the <input> tag are added simply by                                                                    // adding them to this array
				 )
                            
                            
                            )
                    )    
            
            );
            
            
            // initial values of the form 'simulate_detector_form'
	    $this->jform->setDefaultValues(array(
		    'Dataset_File' => "Unknown",
                
	    ));

        
            $forms['simulate_detector_form'] =   clone  $this->jform;
        
            return $forms;
    }
                         
              
    
   
}
