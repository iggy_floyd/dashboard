<?php

/* 
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */


// some trick: Now a model can load  any plugin :-)
class Dashboard_Forms_Model extends TinyMVC_Controller
{

    public $forms=array(
            
        'dashboard_test_form'
        
    );
    
    function get_forms() {


        
        // Now this model can generate forms
        $this->load->library('JForm_Wrapper','jform');
        
        
        // 'prepare_detector_form'
        $this->jform->setForm(
                    array(
			'focus' => "", // focus first element
                        'submit_label' => "Restart",
                        'inputs' => array(                            
                            
                        )                        
                    )                  
            );
        
        
        $forms['dashboard_test_form'] =   clone  $this->jform;
                
                    
        
            return $forms;
    }
                         
              
    
   
}
