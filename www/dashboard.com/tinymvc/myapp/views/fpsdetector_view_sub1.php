
<!--
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */
-->  



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


    <body>

<script type="text/javascript">
     
     
     
    // tail effect
    function tailScroll() {
        var height = $("#log").get(0).scrollHeight;
        $("#log").animate({
        //    scrollTop: '150'
            scrollTop: height
        }, 400);
    }   
    
$(document.body).ready(
        
            
            function(){
                
                $("<div id=\"log\" class=\"ScrollStyle\">Logging</div>").floatingMessage(
                        {
                            position : "bottom-right",
                            padding : 20,
                            height : '250',
                            width: 400
        

                        }
                    );
    
                setInterval(  function(){ 
                        $.get("logs/logs",  null,function(data){       
                            $("#log").html(data.replace(/\n/g,'<br>'))});
                        tailScroll();
                            
                }, 3000);
    
    

});



</script>
    
 
        
        
        <h1>Prepare Detector Test</h1> <br> <br> <br>
    
        <p>
            This test will require to start FPS detector. 
            One needs to open a terminal, and follow instructions below:

            
        <br>
        <br>
        <br>
        <br>
            <pre>
                # presumably, the path to the fps detector is [onshow.fps_detector_path; strconv=no;noerr] 
                cd /path/to/fps-detector/
                python fps_detector.py                

            </pre>
        <br>
        <br>
        <br>
        
    
        <p>
            Afterwards, you can choose a few parameters of the classifiers to be trained, select
            file with JSON data used for training, and define the test/train sample sizes. <br><br>
            When everything is ready, please, click the 'Submit' button. 

        <br>
        <br>
        <br>
        
        <!-- a form goes here-->  
        
        <p> 
            [onshow.prepare_detector_form.begin;strconv=no]  
            <p> 
                <div>
                      [prepare_detector_form.label;strconv=no;block=p] 
                </div>  
                <div>                     
                      [prepare_detector_form.input;strconv=no]   
                </div>  
            </p>
            <p>   
                [onshow.prepare_detector_form.submit.input;strconv=no]  
                [onshow.prepare_detector_form.end;strconv=no] 
            </p>    
        </p> 

        <br> <br> <br>
        
        
        <table border="1" cellpadding="2" cellspacing="0" style="margin-left:auto; margin-right:auto;">
            <tr  style="background-color:#CACACA;">
                <th align="center">Num</th>
                <th align="center">Exception caught</th>

            </tr>

            <tr style="background-color:#E6E6E6;">
                <td>[excpt.#]</td>
                <td>[excpt.type;block=tr;noerr]</td>
        
            </tr>
        </table>    
    </body>
</html>


