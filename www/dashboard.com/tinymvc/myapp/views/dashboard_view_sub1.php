
<!--
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */
-->  



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  
    <script>
       
   function getData() {
    return $.ajax({       
        type: "GET",
        url: 'http://' + window.location.hostname + ':5000/json?callback=?',
        dataType: "jsonp",
        //async: !1,
        error: function() {
            alert("Error occured");
        }
    });
    
    }
    
    function getData2() {
    return $.ajax({       
        type: "GET",
        url: 'http://' + window.location.hostname + ':5000/pandas?callback=?',
        dataType: "jsonp",
        //async: !1,
        error: function() {
            alert("Error occured");
        }
    });
    
    }

    </script>
    
</head>  

 <body>
        <h1>Rickshaw Dashboard Test</h1> <br> <br> <br>
   
        <p>
            This test will require to start a python service providing data
            One needs to open a terminal, and follow instructions below:
        <br>
        <br>
        <br>
        <br>
            <pre>                
                cd /path/to/this/project/bin/
                python server.py              
            </pre>
        <br>
        <br>
        <br>
    
        <!-- our first plot goes here  -->
        
        <style>
        #chart_container {
            position: relative;
            font-family: Arial, Helvetica, sans-serif;
        }
        #chart {
            position: relative;
            left: 40px;
        }
        #y_axis {
            position: absolute;
            top: 0;
            bottom: 0;
            width: 40px;
        }

        <!-- our second plot goes here  -->
        
        #chart_container2 {
            position: absolute;
            font-family: Arial, Helvetica, sans-serif;
        }
        #chart2 {
            position: relative;
            left: 50px;
        }
        #y_axis2{
            position: relative;
            top: 100px;
            bottom: 100px;
            height:100px;
            width: 40px;
        }
         #x_axis2{
            position: relative;
            left: 40px;
            height: 40px;
        }

        </style>

        <!-- our first plot goes here  -->
         
        <div id="chart_container">
            <div id="y_axis"></div>
            <div id="chart"></div>
        </div>
 
      
        <!-- our second plot goes here  -->
        
         <div id="chart_container2">
            <div id="y_axis2"></div>
             <div id="chart2"></div>
            <div id="x_axis2"></div>
            <div id="legend2"></div>

            
        
        </div>
            
            
        <script type="text/javascript" src="javascripts/rickshaw/vendor/d3.min.js"></script>
        <script type="text/javascript" src="javascripts/rickshaw/vendor/d3.layout.min.js"></script>
        <script type="text/javascript" src="javascripts/rickshaw/rickshaw.min.js"></script>
        <link rel="stylesheet" href="javascripts/rickshaw/rickshaw.min.css">
        <script>   
            
            function control_plot1(tv) {
            

             // instantiate our graph!
            var graph = new Rickshaw.Graph( {
                element: document.querySelector("#chart"),
                width: 900,
                height: 200,
                min: 'auto',
                renderer: 'line',
                series: new Rickshaw.Series.FixedDuration([{ name: 'one' }], undefined, {
                    timeInterval: tv,
                    maxDataPoints: 100,
                    timeBase: new Date().getTime()/(tv),
                    //yScale:d3.scale.linear().domain([-2.,2.]).nice()
                }) 
            });


            var axes = new Rickshaw.Graph.Axis.Time( { graph: graph } );
            var y_axis = new Rickshaw.Graph.Axis.Y.Scaled({
                element: document.getElementById('y_axis'),
                graph: graph,
                grid: false,
                orientation: 'left',
                //scale:d3.scale.linear().domain([-3.,3.]).range([0.,6]),
                //scale:d3.scale.linear().domain([-3.,3.]).range([0.,3]),
                scale:d3.scale.linear(),
                tickFormat: Rickshaw.Fixtures.Number.formatKMBT
            });

   
            graph.render();  

            function update() {
            var ajaxObj = getData();
                        
            ajaxObj.success(    function(response) {          
              var  data= { one:response.y};
              graph.series.addData(data);                
                                       
             graph.render();
             
             graph.update();
            });
        }
            setInterval( function() {

                    update();

            }, tv );
            } 
            
            function control_plot2(tv) {
               
           
                var data2 = [ ];              
                    
                     var graph2 = new Rickshaw.Graph( {
                    element: document.querySelector("#chart2"),
                    width: 800,
                    height: 100,                    
                    min: 'auto',
                    renderer: 'line',                  
                    series: [ {
                        color: 'steelblue',
                        data: data2,
                        name: "Test"
                    } ]
                    });
                     

                    var x_axis2 = new Rickshaw.Graph.Axis.X( { 
                        graph: graph2, 
                        orientation: 'bottom', 
                        element: document.getElementById('x_axis2'), 
                        pixelsPerTick: 200
                        
                            }); 
                             
                    var y_axis2 = new Rickshaw.Graph.Axis.Y.Scaled( { 
                        graph: graph2, 
                        orientation: 'left', 
                        pixelsPerTick: 20,
                        grid:false,
                        tickFormat: Rickshaw.Fixtures.Number.formatKMBT, 
                        element: document.getElementById('y_axis2'),
                        scale:d3.scale.linear().domain([0.,1.]).range([0.,1])
                        } );
                                        

                    graph2.render(); 
                    
                function update2() {
                    var ajaxObj2 = getData2();
                    ajaxObj2.success(function(response) {                                  
                      for (i = 0; i < response.length; i++) {
                          data2.push({x:response[i].x,y:response[i].y});  
                      }
     
                        //console.log(response[0].x);
                        //console.log(data2.length);
      
                        $(graph2.series).each(function(i){
                          graph2.series[i].name = response[0].name;
                          
                        });
                        
                        
                        $('#legend2').empty();
                        var legend = new Rickshaw.Graph.Legend({
                            graph: graph2,
                            element: document.getElementById('legend2')
                            });
                        
                        
                        graph2.update();
                        
                        data2.splice(0,data2.length);
                        

                    });
                }
                setInterval( function() {

                    update2();

                }, tv );
       
            }
       
            control_plot1(1000);
            control_plot2(1000);
       
       
       
       
        </script>           
        <br>
        <br>
        <br>
        
        <!-- a form goes here-->  
        
        <p> 
            [onshow.dashboard_test_form.begin;strconv=no]  
            <p> 
                <div>
                      [dashboard_test_form.label;strconv=no;block=p] 
                </div>  
                <div>                     
                      [dashboard_test_form.input;strconv=no]   
                </div>  
            </p>
            <p>   
                [onshow.dashboard_test_form.submit.input;strconv=no]  
                [onshow.dashboard_test_form.end;strconv=no] 
            </p>    
        </p> 

        <br> <br> <br>
        
        
        <table border="1" cellpadding="2" cellspacing="0" style="margin-left:auto; margin-right:auto;">
            <tr  style="background-color:#CACACA;">
                <th align="center">Num</th>
                <th align="center">Exception caught</th>

            </tr>

            <tr style="background-color:#E6E6E6;">
                <td>[excpt.#]</td>
                <td>[excpt.type;block=tr;noerr]</td>
        
            </tr>
        </table>    
    </body>
</html>


