
<!--
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */
-->  



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
     <script type="text/javascript" src="javascripts/vis.min.js"></script>
     <link rel="stylesheet" href="javascripts/vis.min.css">
     
</head>    
    

 <body>
        <h1>Vis.js Dashboard Test</h1> <br> <br> <br>
   
        <p>
            This test will require to start a python service providing data
            One needs to open a terminal, and follow instructions below:
        <br>
        <br>
        <br>
        <br>
            <pre>                
                cd /path/to/this/project/bin/
                python server.py              
            </pre>
        <br>
        <br>
        <br>
    
        
        
        <style>
        #chart3 {
            position: relative;
            left: 40px;
        }
        #chart4 {
            position: relative;
            left: 40px;
        }        
        </style>        
 
        <!-- our first plot goes here  -->        
        <div id="chart3"></div>
        <!-- our second plot goes here  -->
        <div id="chart4"></div>
 
        <script>  
        function control_plot3(tv) {
            
        // create a graph2d with an (currently empty) dataset
        var container = document.getElementById('chart3');
        var dataset = new vis.DataSet();
        
        
        
        var options = {
            start: vis.moment().add(-30, 'seconds'), // changed so its faster
            end: vis.moment(),
            dataAxis: {
                left: {
                  range: {
                    min:-10, max: 10
                  }
                }
            },
            drawPoints: {
                style: 'circle' // square, circle
            },
            shaded: {
                orientation: 'bottom' // top, bottom
            }
        };
        
        
        // our graph
        var graph2d = new vis.Graph2d(container, dataset, options);
        
        
        // our function rendering graphics
        function renderStep() {
        // move the window (you can think of different strategies).
            var now = vis.moment();
            var range = graph2d.getWindow();
            var interval = range.end - range.start;
            graph2d.setWindow(now - interval, now, {animation: false});
            //setTimeout(renderStep, tv);              
        }
        
        
        
        function update() {
                var ajaxObj = getData();                       
                ajaxObj.success(    function(response) {   
                    
                var now = vis.moment();
                dataset.add({
                    x: now,
                    y: response.y
                });    
                    
                // remove all data points which are no longer visible
                var range = graph2d.getWindow();
                var interval = range.end - range.start;
                var oldIds = dataset.getIds({
                    filter: function (item) {
                        return item.x < range.start - interval;
                    }
                });
                
                dataset.remove(oldIds);
                renderStep();
            });
            }
         setInterval( function() { update();}, tv );            
        }
        
        function control_plot4(tv) {
            
        // create a graph2d with an (currently empty) dataset
        var container = document.getElementById('chart4');
        var dataset = new vis.DataSet();
        var options  = {};
        var groups = new vis.DataSet();
        var graph2d = new vis.Graph2d(container, dataset, groups,options);
        
        
         
        function update() {
                var ajaxObj = getData2();                       
                ajaxObj.success(    function(response) { 
                    
                    
                    var options = {
                            start: response[0].x,
                            end:  response[response.length-1].x,                            
                            legend: {left:{position:"bottom-left"}},
                            showMinorLabels:true,
                            timeAxis:{scale:'millisecond'},                            
                            showMajorLabels:false
                        };
                    
                    
                    dataset.clear();
                    groups.clear();
                    groups.add({
                        id: 0,
                        content:response[0].name ,                        
                        options: {
                            drawPoints: {
                                style: 'square' // square, circle
                            },
                            shaded: {
                                orientation: 'bottom' // top, bottom
                            }
                        }});
            
                    for (i = 0; i < response.length; i++) {
                          dataset.add({x:response[i].x,y:response[i].y,group:0});  
                      }
                    
                    graph2d.setGroups(groups);
                    graph2d.setOptions(options);
                    graph2d.setItems(dataset);
                
            });
            }
        setInterval( function() { update();}, tv );    
        
        }
        
        
        control_plot3(100);
        control_plot4(1000);
        </script>  
              
        <br>
        <br>
        <br>
        
       
       
    </body>
</html>


