
<!--
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */
-->  



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


    <body>


    
 
        
        
        <h1>Simulate Detector Test</h1> <br> <br> <br>
    
        <p>
            This test will require to start FPS detector. 
            One needs to open a terminal, and follow instructions below:

            
        <br>
        <br>
        <br>
        <br>
            <pre>
                # presumably, the path to the fps detector is [onshow.fps_detector_path; strconv=no;noerr] 
                cd /path/to/fps-detector/
                python fps_detector.py                

            </pre>
        <br>
        <br>
        <br>
        
        <p>
            Then, you need to 'Prepare' FPS detector running commands from the 'Prepare Detector Test' tab. <br><br>
            When everything is ready, please, choose the JSON file used to simulate the detector and click the 'Submit' button. 
        
    
        <br>
        <br>
        <br>
        
        <!-- a form goes here-->  
        
        <p> 
            [onshow.simulate_detector_form.begin;strconv=no]  
            <p> 
                <div>
                      [simulate_detector_form.label;strconv=no;block=p] 
                </div>  
                <div>                     
                      [simulate_detector_form.input;strconv=no]   
                </div>  
            </p>
            <p>   
                [onshow.simulate_detector_form.submit.input;strconv=no]  
                [onshow.simulate_detector_form.end;strconv=no] 
            </p>    
        </p> 

        <br> <br> <br>        
     
        
    
        <table border="1" cellpadding="2" cellspacing="0" style="margin-left:auto; margin-right:auto;">
            <tr  style="background-color:#CACACA;">
                <th align="center">Num</th>
                <th align="center">Exception caught</th>

            </tr>

            <tr style="background-color:#E6E6E6;">
                <td>[excpt2.#]</td>
                <td>[excpt2.type;block=tr;noerr]</td>
        
            </tr>
        </table>        
      
    </body>
</html>


