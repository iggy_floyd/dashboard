## Abstract

''' @2014 I. Marfin ''' ''&lt;Unister Gmbh&gt;''

This is a simple dashboard utilizing several javascript libraries such as

  * ''D3.js''
  * ''Rickshaw''
  * ''Vis.js''
  * ''jQuery''

to communicate with any server having JSON REST API.

In this package, the python-based ''Flask'' server is used, to process the
data in the 'data/' folder, make some calculation (using pandas dataframe and
numpy histogram, it produce some distributions), and return the results to the
dashboard.

## Brief HOW-TO

  * Testing your system to have needed components and building the documentation

make

If the command fails, please, install all needed programs and python modules.
Some python modules such as

  * creole
  * html2text

are optional. They can be excluded from ''configure.ac''.

  * Starting the dashboard (&lt;tt&gt;Terminal #1&lt;/tt&gt;)

make serve dashboard

  * Starting the flask server (&lt;tt&gt;Terminal #2&lt;/tt&gt;)

cd bin/ python server.py

  * Generation of the documentation HTML and Markdown pages 

make doc

That's it.

## Result

'''Fig. 1''' Example of the dashboard based on Rickshaw

![Rickshaw Dashboard](https://bytebucket.org/iggy_floyd/dashboard/raw/1170818f0c002a5f8089680926fb1e2335eb7505/screencast2gif/GIFrecord_2015-09-25_130930.gif)

'''Fig. 2''' Example of the dashboard based on Vis.js

![Vis.js Dashboard](https://bytebucket.org/iggy_floyd/dashboard/raw/1170818f0c002a5f8089680926fb1e2335eb7505/screencast2gif/GIFrecord_2015-09-25_131057.gif)

## Contact

Igor Marfin ''&lt;Igor.Marfin@unister-gmbh.de&gt;''

