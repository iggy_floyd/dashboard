#!/usr/bin/php

<?php
require_once( __DIR__ . '/vendor/autoload.php' );

if ( $argc > 1 ) {
   $argument1 = $argv[1];

   $wikitext =  file_get_contents($argument1);
   $converter = new WikitextToMarkdown\Converter();
   $markdown = $converter->convert( $wikitext );
   echo $markdown;
} else {
     echo "Usage: ".__FILE__." file.wiki\n";
     exit;
}


?>
