<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'WikitextToMarkdown\\' => array($baseDir . '/src'),
    'Symfony\\Component\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher'),
    'Guzzle\\Service\\Mediawiki\\UnitTests\\' => array($vendorDir . '/addwiki/guzzle-mediawiki-client/tests/unit'),
    'Guzzle\\Service\\Mediawiki\\IntegrationTests\\' => array($vendorDir . '/addwiki/guzzle-mediawiki-client/tests/integration'),
    'Guzzle\\Service\\Mediawiki\\' => array($vendorDir . '/addwiki/guzzle-mediawiki-client/src'),
);
