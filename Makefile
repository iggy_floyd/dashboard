

# @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
# simple makefile to manage this project



all: configuration doc


# to check that the system has all needed components
configuration: configure
	@./configure


# to check an update in the configure.ac. If it's found, update the 'configure' script.
configure: configure.ac
	@./configure.ac



doc: README.wiki 
	-@ mkdir doc 2>/dev/null
	-@ ls README.wiki | sed -ne 's/.wiki//p' | xargs -I {}  echo "wiki-tool/mediawiki2texi.py {}.wiki {}.info {} >{}.texinfo; makeinfo --force --html {}.texinfo; makeinfo {}.texinfo; cat {}.info" | sh
	-@ rm *info
	-@ ls README.wiki | sed -ne 's/.wiki//p' | xargs -I {}  echo "cp {}/index.html doc/index.html; rm -r {}" | sh
	-@ wiki2md-tool/mw2md.py  README.wiki  README.md


test: 
	-@ echo "Not implemented"



run: 
	-@ echo "Not implemented"


build: 
	-@ echo "Test suites:"


# to clean all temporary stuff
clean:  
	-@rm -r config.log autom4te.cache
	-@rm -r doc


# to serve the tinyMVC framework with our dashboard	
ifeq (serve,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  SERVE_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(SERVE_ARGS):;@:)
endif


serve:
	-@firefox localhost:8010/$(SERVE_ARGS) &
	-@php -S localhost:8010 -t www/dashboard.com/public


.PHONY: configuration clean all doc run test  clean serve test build
