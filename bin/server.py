#!/usr/bin/env python
#
# -*- coding: utf-8 -*-
#
# Server
# ==================================================================
#
# :Date: Sep 24, 2015, 9:28:51 AM
# :File:   server.py
# :Copyright:  @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
#
#
#
# It generates the Gaussian Random Numbers





'''
    A server, which generates the Gaussian Random Numbers
'''


# Authors and Version
# ------------------------
#
# ::



__author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
__date__ ="$Sep 24, 2015 9:28:51 AM$"
__docformat__ = 'restructuredtext'
__version__ = "0.0.1"



# Requirements
# ------------
#
# ::

import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9

from flask import Flask,request, current_app
from functools import wraps
import json
import numpy as np
import time
import pandas as pd
import random



# Pandas dataframe
# --------------------------------------------
# This dataframe is used to provide some data
# ::

dataframe=pd.read_csv('../data/FL_insurance_sample.csv')
constructions = pd.Series(dataframe['construction'].values.ravel()).unique();

# JSONP decorator
# --------------------------------------------
# ::



def jsonp(func):
    """Wraps JSONified output for JSONP requests."""
    @wraps(func)
    def decorated_function(*args, **kwargs):
        callback = request.args.get('callback', False)
        if callback:
            #data = str(func(*args, **kwargs).data)
            data = str(func(*args, **kwargs))
            content = str(callback) + '(' + data + ')'
            mimetype = 'application/javascript'
            return current_app.response_class(content, mimetype=mimetype)
        else:
            return func(*args, **kwargs)
    return decorated_function


# Flask server application
# --------------------------------------------
# ::

app = Flask(__name__)


# the `/json` route
# ~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# the ``json`` route returns 'y' -- the randomly normal distributed  value per time
#


@app.route("/json")
@jsonp
def route_json():
    ''' the ``json`` route returns 'y' -- the randomly normal distributed  value.'''
    return json.dumps({'x':int(time.time()),'y':np.random.normal()})



# the `/pandas` route
# ~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# the ``pandas`` route returns randomly distribution of the 'tiv_2011' category from the file 'FL_insurance_sample.csv'  
#


@app.route("/pandas")
@jsonp
def route_pandas():
    '''the ``pandas`` route randomly on 'construction' returns  a distribution of the 'tiv_2011' category from the file 'FL_insurance_sample.csv' '''
    
    
    construction = random.choice(constructions)
    data=dataframe.loc[dataframe[dataframe['construction'] == construction].index ,['tiv_2011']].as_matrix()    
    data /=10000; 
    vals, bins= np.histogram(data, normed=True)
    binWidth = bins[1] - bins[0]
    vals *= binWidth        
    centers = (bins[:-1] + bins[1:]) / 2
    merged= zip(centers,vals)
    merged_dict= [ {'x':x,'y':y,'name':construction} for (x,y) in merged] 
    
    return json.dumps(merged_dict)
    
    
    


# The main entrance point of the server.py
# ----------------------------------------------------------------
#
#  .. code-block:: bash
#
#                  #Terminal 1
#                  ~/project/bin> python server.py
#
#                  #Terminal 2
#                  curl -X GET localhost:5000/json
#
# ::

if __name__ == "__main__":
    
    
    #print dataframe.head(10)
    #print dataframe.tail(10)
    #print dataframe.tail(10)
    '''
    print dataframe['construction']
    constructions =  pd.Series(dataframe['construction'].values.ravel()).unique()
    construction = random.choice(constructions)
    print construction
    data=dataframe.loc[dataframe[dataframe['construction'] == construction].index ,['tiv_2011']].as_matrix()    
    data /=100; 
    vals, bins= np.histogram(data, normed=True)
    binWidth = bins[1] - bins[0]
    print vals
    print vals*binWidth
    print bins
    center = (bins[:-1] + bins[1:]) / 2
    print center
    merged= zip(center,vals*binWidth)
    print merged
    merged_dict= [ {'x':x,'y':y} for (x,y) in merged] 
    print json.dumps(merged_dict)
    
    import matplotlib.pyplot as plt    
    plt.bar(bins[:-1], vals*binWidth, binWidth) 
    plt.show()
    '''
    
        
    app.run()    
